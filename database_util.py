import psycopg2
from psycopg2 import sql


def connectToDB():
    host = "holmz-dev.postgres.database.azure.com"
    port = 5432
    dbname = "holmz-uat"
    user = "devDBAdmin"
    password = "gzM26qjtqIb3xyh"
    sslmode = "require"

    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


conn = connectToDB()


def makeOverallResultTable():
    with conn.cursor() as curs:
        curs.execute("""
            SELECT EXISTS (SELECT * FROM information_schema.tables
            WHERE table_name=%s)
        """, ['overallresult'])
        if not curs.fetchone()[0]:
            curs.execute("""
                CREATE TABLE public.overallresult (id int, date timestamp,
                hipFlexorLeft int, hipFlexorRight int, gluteLeft int,
                gluteRight int, adductorLeft int, adductorRight int,
                abductorLeft int, abductorRight int, quadricepLeft int,
                quadricepRight int, hamstringLeft int, hamstringRight int,
                gastrocnemiousLeft int, gastrocnemiousRight int,
                soleusLeft int, soleusRight int, footLeft int,
                footRight int, rsiRiskMessage text, balance float,
                stability float, motionQuality float, rtp float,
                fallRisk float, rsiRisk float, balanceGoal float,
                stabilityGoal float, motionQualityGoal float,
                rtpGoal float, fallRiskGoal float, rsiRiskGoal float)
            """)


def makeFlexionTable():
    with conn.cursor() as curs:
        curs.execute("""
            SELECT EXISTS (SELECT * FROM information_schema.tables
            WHERE table_name=%s)
        """, ['flexion'])
        if not curs.fetchone()[0]:
            curs.execute("""
                CREATE TABLE public.flexion (id int, date timestamp,
                ankleInversionLeft float, ankleInversionRight float,
                ankleEversionLeft float, ankleEversionRight float,
                footMTPFlexionLeft float, footMTPFlexionRight float,
                hipAbductionLeft float, hipAbductionRight float,
                hipAdductionLeft float, hipAdductionRight float)
            """)


def makeProgramResultsTable():
    pass


def makeProgamScheduleTable():
    pass
