from typing import Generic, TypeVar
from pydantic.generics import GenericModel
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import AsIs
import random
from jose import jwt, JWTError
from passlib.context import CryptContext
from fastapi import FastAPI, WebSocket, HTTPException, Request, status, Depends, Header
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi_versioning import VersionedFastAPI, version
from datetime import date, datetime, timedelta
from pydantic import BaseModel, conlist, Field
from pydantic_collections import BaseCollectionModel
from fastapi.responses import JSONResponse
from typing import Annotated
from database_util import makeOverallResultTable, makeFlexionTable
import openai
import re

app = FastAPI(title="Holmz Backend")


def connectToDB():
    host = "holmz-dev.postgres.database.azure.com"
    dbname = "holmz-uat"
    user = "devDBAdmin"
    password = "gzM26qjtqIb3xyh"
    sslmode = "require"

    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn


conn = connectToDB()
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
ALGORITHM = "HS256"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
oauth2_secret = 'E5gDh3T56c792k7BUNd455TL7'
openai.api_key = "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"


@app.post("/")
@version(1, 1)
async def root():
    return {"message": "Hello World"}


class PatientID(BaseModel):
    id: int


class PTID(BaseModel):
    id: int


class Exercise(BaseModel):
    name: str = ''
    id: int = -1
    image_url: str = ''
    program_id: int = -1
    program_image: str = ''
    animation: str = ''
    type: str = 'Functional'
    description: str | None = None
    sets: int = -1
    reps: int = -1
    setsCompleted: int = -1
    repsCompleted: int = -1


class ExerciseList(BaseCollectionModel[Exercise]):
    pass


class ProgramResults(BaseModel):
    patientid: PatientID
    date: date
    recoveryProgressCurrent: float
    recoveryProgressGoal: int
    recoveryProgressDailyDifference: float
    adherenceCurrent: float
    adherenceGoal: int
    adherenceProgressDailyDifference: float
    motionQualityCurrent: float
    motionQualityGoal: int
    motionQualityDailyDifference: float
    scheduledProgram: ExerciseList = ExerciseList()
 

class Program(BaseModel):
    startDate: date
    endDate: date
    exercises: ExerciseList


class Report(BaseModel):
    abstract: str = ''
    data: str = ''
    methodology: str = ''
    conclusion: str = ''
    recommendation: str = ''
    definitions: str = ''


class RangeOfMotionAssessment(BaseModel):
    ankleInversionLeft: int
    ankleInversionRight: int
    ankleEversionLeft: int
    ankleEversionRight: int
    footMTPFlexionLeft: int
    footMTPFlexionRight: int
    hipAbductionLeft: int
    hipAbductionRight: int
    hipAdductionLeft: int
    hipAdductionRight: int
    feasibilityRed: int = 90
    feasibilityYellow: int = 80


class LowerExtremityRisk(BaseModel):
    hipFlexorLeft: int
    hipFlexorRight: int
    gluteLeft: int
    gluteRight: int
    adductorLeft: int
    adductorRight: int
    abductorLeft: int
    abductorRight: int
    quadricepLeft: int
    quadricepRight: int
    hamstringLeft: int
    hamstringRight: int
    gastrocnemiousLeft: int
    gastrocnemiousRight: int
    soleusLeft: int
    soleusRight: int
    footLeft: int
    footRight: int


class PatientOverallResult(BaseModel):
    patient_id: PatientID
    lowerExtremityRSIRisk: LowerExtremityRisk
    rsiRiskMessage: str
    balance: float
    stability: float
    motionQuality: float
    rtp: float
    fallRisk: float
    rsiRisk: float
    balanceGoal: float
    stabilityGoal: float
    motionQualityGoal: float
    rtpGoal: float
    fallRiskGoal: float
    rsiRiskGoal: float


class ExerciseGraphPoint(BaseModel):
    performance: str
    progress_start: float
    progress_end: float


class BalanceDiagramPoint(BaseModel):
    x: float
    y: float


class ExerciseGrade(BaseModel):
    patient_id: PatientID
    patient_status: str = 'offline'
    exercise_name: str
    exercise_progress_graph: list[ExerciseGraphPoint]
    total_sec: int
    number_of_sets: int
    balance_diagram: list[BalanceDiagramPoint]
    balance: float
    stability: float
    motionQuality: float
    rtp: float
    fallRisk: float
    rsiRisk: float


class Point(BaseModel):
    x: float
    y: float
    z: float
    grade: int = -1


class SkeletonPointList(BaseModel):
    points: conlist(Point, min_items=20, max_items=20)


class ExerciseSkeleton(BaseModel):
    frames: list[SkeletonPointList]


class RTP(BaseModel):
    fallRisk: float
    accuracy: float
    strength: float
    stamina: float
    stability: float
    speed: float
    precision: float
    power: float
    posture: float
    flexibility: float
    exertion: float
    endurance: float
    coordination: float
    control: float
    balance: float
    agility: float
    fatigue: float
    load: float
    metabolicCost: float
    mskStress: float


class Patient(BaseModel):
    patient_id: PatientID
    firstName: str
    lastName: str
    profileImage: str | None = ''
    programResults: ProgramResults | None
    startTreatment: bool = True
    new_patient: bool = False
    active_patient: bool = True
    recovery_progress: float
    adherence: float
    motion_quality: float


class GroupOverview(BaseModel):
    organizationName: list[str]
    cohortName: str
    organizationImage: str
    patientList: list[Patient]


class GroupSearch(BaseModel):
    search: str


class GroupFilter(BaseModel):
    filter: int


class Organizations(BaseModel):
    name: str
    image: str


class CohortTrendAnalytics(BaseModel):
    usagePercentage: int


DataT = TypeVar('DataT')


class Response(GenericModel, Generic[DataT]):
    success: bool = True
    message: str = ""
    data: DataT
    pages: int = 1


Model = TypeVar("Model", bound=BaseModel)


class PaginationData(GenericModel, Generic[Model]):
    index: int = 0
    page: int = 1
    total: int = 1
    size: int = 1
    items: list[Model]


class PaginationModel(GenericModel, Generic[Model]):
    message: str = 'success'
    success: bool = True
    data: PaginationData[Model] or list


async def getPatientFromDB(id):
    with conn.cursor() as curs:
        curs.execute(
            """SELECT * FROM public.user
            WHERE id = (SELECT user_id FROM public.patient
            WHERE id = %s);""", [id.id]
        )
        data = curs.fetchone()
        # Missing ID returns None Type
        return data


async def getPTFromDB(id):
    with conn.cursor() as curs:
        curs.execute(
            """SELECT * FROM public.user
            WHERE id = (SELECT user_id FROM public.physician
            WHERE id = %s);""", [id.id]
        )
        data = curs.fetchone()
        return data


async def getPatientListFromDB(id):
    with conn.cursor() as curs:
        curs.execute(
            """SELECT patient_id, first_name, last_name,
            is_treatment_started, is_active_patient
            FROM
            public.patientmapping
            LEFT JOIN public.patient
            ON public.patient.id = public.patientmapping.patient_id
            LEFT JOIN public.user
            ON public.patient.user_id = public.user.id
            WHERE physician_id = %s;""", [id]
        )
        data = curs.fetchall()
        # print(data)
        patients = []
        if data:
            for patient in data:
                patients.append(
                    Patient(
                        patient_id=PatientID(id=patient[0]),
                        firstName=patient[1],
                        lastName=patient[2],
                        profileImage='',
                        startTreatment=patient[3],
                        new_patient=random.choice([True, False]),
                        active_patient=patient[4],
                        recovery_progress=random.uniform(0.7, 1),
                        adherence=random.uniform(0.7, 1),
                        motion_quality=random.uniform(0.7, 1),
                        programResults=None,
                    )
                )
        return patients


async def getPatientDataFromTable(id, table, altTitle=None):
    with conn.cursor() as curs:
        if not altTitle:
            curs.execute(
                sql.SQL("SELECT * FROM public.{} WHERE id = %s")
                .format(sql.Identifier(table)),
                [id.id])
        else:
            curs.execute(
                sql.SQL("SELECT * FROM public.{table} WHERE {field} = %s")
                .format(
                    table=sql.Identifier(table),
                    field=sql.Identifier(altTitle)
                ), [id]
            )
        data = curs.fetchone()
        return data


async def cacheDataInDB(table:str, dataToCache:dict):
    table = 'test'
    dataToCache = {
        'column': 'data'
    }

    query = sql.SQL("INSERT INTO public.{table} (%s) VALUES %s").format(
            table=sql.Identifier(table),
    )
 
    with conn.cursor() as curs:
        curs.execute(
            curs.mogrify(
                query, 
                (AsIs(','.join(dataToCache.keys()) ,dataToCache.values() ))
            )
        )
    status = 400
    return status


async def getCurrentUser(token: Annotated[str, Depends(oauth2_scheme)]):
    try:
        payload = jwt.decode(token, oauth2_secret, algorithms=[ALGORITHM])
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid Credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return payload


async def getCurrentActiveUser(
        currentUser: Annotated[bool, Depends(getCurrentUser)]):
    return currentUser


@app.get('/users/me')
async def read_users_me(current_user: Annotated[str, Depends(getCurrentUser)]):
    return current_user


def isTokenValid(token: str):
    data = None
    try:
        data = jwt.decode(token, oauth2_secret, algorithms=[ALGORITHM])
    except JWTError:
        return False
    if not data:
        return False
    return True


@app.post('/token')
@version(1, 2)
async def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]):
    user = await getPatientDataFromTable(
            form_data.username, 'user', altTitle='email')
    if not user:
        raise HTTPException(status_code=400,
                            detail='Incorrect Username or Password')
    if not pwd_context.verify(form_data.password, user[7]):
        raise HTTPException(status_code=400,
                            detail='Incorrect Username or Password')
    toToken = {"sub": form_data.username,
               "exp": datetime.utcnow() + timedelta(minutes=60)}
    token = jwt.encode(toToken, oauth2_secret, algorithm=ALGORITHM)
    payload = {"access_token": token, "token_type": "bearer"}
    return payload


@app.post('/getPatientTodaysProgramResults', summary="For a patient ID, retrieve the patient's Recovery, Progress, Adherence, Motion Quality, and Exercise Grading", response_description="", response_model=PaginationModel[ProgramResults or list], tags=['Routes', 'PT', 'Patient'])
@version(1, 2)
async def getPatientTodaysProgramResults(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)
    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )
    recoveryProgressCurrent = 0
    recoveryProgressDailyDifference = 0
    adherenceCurrent = 0
    adherenceProgressDailyDifference = 0
    motionQualityCurrent = 0
    motionQualityDailyDifference = 0

    # check if patient data is in the database already
    # data = await getPatientDataFromTable(patientID, 'programschedule')
    data = await getPatientDataFromTable(patientID.id, 'programschedule', altTitle='patient_id')
    exerciseList = []

    # if it isn't generate it and store it in the database and reply
    # id, patient id, program id, goal reps, goal sets, repeat every, repeat on, end on, ideal joint 
    if data:
        recoveryProgressCurrent = data[2]
        recoveryProgressDailyDifference = random.randint(-5, 5)
        adherenceCurrent = data[3]
        adherenceProgressDailyDifference = random.randint(-5, 5)
        motionQualityCurrent = data[4]
        motionQualityDailyDifference = random.randint(-5, 5)
        print('data 45: ', data[4:6])
        for exercise in range(1):
            exerciseList.append(Exercise(
                id=1,
                program_id=data[2],
                program_image='',
                name='',
                description='',
                sets=int(data[4]),
                reps=int(data[3]),
                setsCompleted=random.randint(0,2),
                repsCompleted=random.randint(0,8),
            ))
    else:
        recoveryProgressCurrent = random.randint(50, 100)
        recoveryProgressDailyDifference = random.randint(-10, 10)
        adherenceCurrent = random.randint(50, 100)
        adherenceProgressDailyDifference = random.randint(-10, 10)
        motionQualityCurrent = random.randint(50, 100)
        motionQualityDailyDifference = random.randint(-10, 10)
        exerciseList.append(Exercise(
            id=0,
            program_id=0,
            program_image='None',
            name='None',
            description='None',
            sets=0,
            reps=0,
            setsCompleted=0,
            repsCompleted=0,
        ))

    exerciseList = ExerciseList(exerciseList)

    return PaginationModel(data=PaginationData(
        index=0,
        size=1,
        total=1,
        items=[
            ProgramResults(
                patientid=patientID,
                date=date.today(),
                recoveryProgressCurrent=recoveryProgressCurrent,
                recoveryProgressGoal=70,
                recoveryProgressDailyDifference=recoveryProgressDailyDifference,
                adherenceCurrent=adherenceCurrent,
                adherenceGoal=70,
                adherenceProgressDailyDifference=adherenceProgressDailyDifference,
                motionQualityCurrent=motionQualityCurrent,
                motionQualityGoal=70,
                motionQualityDailyDifference=motionQualityDailyDifference,
                scheduledProgram=exerciseList
            )
        ]
    ))


@app.post('/createPatientProgram', summary="For a patient ID, generate an exercise program for the individual", response_description="", response_model=PaginationModel[Program], tags=['Routes', 'Patient'])
@version(1, 2)
async def createPatientProgram(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)
    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )
    return PaginationModel(data=PaginationData(
        index=0,
        size=1,
        total=1,
        items=[
            Program(
                startDate=datetime(year=2022, month=random.randint(
                    1, 12), day=random.randint(1, 28)),
                endDate=datetime(year=2023, month=random.randint(
                    1, 12), day=random.randint(1, 28)),
                exercises=ExerciseList(
                    [
                        Exercise(
                            id=0,
                            program_id=1,
                            image_url="",
                            name="One Leg Hop",
                            description="",
                            sets=3,
                            reps=8,
                            setsCompleted=0,
                            repsCompleted=0
                        ),
                        Exercise(
                            id=1,
                            program_id=1,
                            image_url="",
                            name="Box Step Up",
                            description="",
                            sets=2,
                            reps=4,
                            setsCompleted=0,
                            repsCompleted=0
                        ),
                        Exercise(
                            id=2,
                            program_id=1,
                            image_url="",
                            name="Triple Leg Hop",
                            description="",
                            sets=3,
                            reps=2,
                            setsCompleted=0,
                            repsCompleted=0
                        )
                    ]
                )
            )
        ]
    ))


@app.post('/websocket/connect', summary="Connect to the websocket for live patient data upload and download during exercises", response_description="", response_model=None, tags=['Routes', 'Patient'])
@version(1, 1)
async def websocket(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID, websocketCredentials: WebSocket):
    await websocketCredentials.accept()
    while True:
        data = await websocketCredentials.receive_bytes()
        # await websocket.send_bytes()
    # arr = [patientID]
    # return arr


@app.post('/getPatientReport', summary="For a patient ID, generate a readiness to perform and RSI risk report", response_description="", response_model=PaginationModel[Report], tags=['Routes', 'PT'])
@version(1, 1)
async def getPatientReport(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)
    # print(patient)
    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )
    prompt = '''Please write a 5 section report about an individual\'s lower extremity musculoskeletal
    health with the 5 sections labeled as ABSTRACT, DATA, METHODOLOGY, RECOMMENDATION, and CONCLUSION. The
    individual is generally healthy with no major issues except for a slight tendency to over rely on their
    left knee.'''

    payload = {
        'model': 'gpt-3.5-turbo',
        'messages': [{'role': 'user', 'content': prompt}],
        'temperature': 0.1,
        'max_tokens': 1000,
    }
    response = await openai.ChatCompletion.acreate(**payload)
    content = response.choices[0].message.content

    splitList = re.split(r'[A-Z]*:\n', content)
    splitList = list(filter(None, splitList))
    segments = {}
    if len(splitList) == 4:
        segments = {
            'abstract': splitList[0],
            'data': splitList[1],
            'method': splitList[2],
            'recommendation': splitList[3],
            'conclusion': splitList[4]
        }
    elif len(splitList) > 4:
        segments = {
            'abstract': splitList[0],
            'data': splitList[1],
            'method': splitList[2],
            'recommendation': ''.join(splitList[3:-1]),
            'conclusion': splitList[-1]
        }
    definitions = """Component	Definition
Accuracy	How close an individual is in form to the ideal form for a given activity
Agility	Ability to minimize transition time and efficiency from one point to another
Balance	Ability to control the placement of the body's center of gravity in relation to its support base
Biomechanical Load	Forces, load moments, and muscular power output acting on the body and joints
Control	Ability to align movement for a given activity with the ideal form for that movement and activity e.g., professional ballroom dancing, ice skating
Coordination	Ability to combine several distinct movement patterns into a singular distinct movement
Determination	A positive emotional feeling that involves persevering to a difficult goal in spite of obstacles; motivating behavior that will help achieve one's goals
Endurance	Ability to exert and remain active for a long period of time as well as having the ability to resist withstand and recover from fatigue; physiological capability to recover from fatigue; physiological capability
Exertion	Amount of work performed during an activity
Fatigue	Degradation of form and performance
Flexibility	Range of motion of joints
Metabolic Cost	The amount of energy consumed as athe results of performing a given activity usually measured in calories (kcal) or metabolic equivalent (MET)
Posture	Attitude assumed by the body either with support during muscular inactivity or by means of the coordinated action of many muscles working to maintain stability
Power	Ability of a muscular units) to apply maximum force in a period of time e.g., watts per hour
Precision	How consistent an individual is in form regardless of accuracy. how close two or more measurements are to each other
Speed	Maximum speed and acceleration of an individual
Stability	Maintain balance with minimal need for correction over time and activity; aligns with preferred motion (reduces risk of injury)
Stamina	Physical and mental strength that allows you to continue doing something for a long time; determination physiological irregularities, psychological and environmental impacts on endurance
Strength	Ability of a muscular units) to apply force
Training Load	Measure of intensity during a training session or how much stress is put on the body
Training Volume	Amount of training in a given time period"""

    return PaginationModel(data=PaginationData(items=[
        Report(
            abstract=segments["abstract"],
            data=segments["data"],
            methodology=segments["method"],
            conclusion=segments["conclusion"],
            recommendation=segments["recommendation"],
            definitions=definitions,
        )
    ]))


@app.post('/getPatientRangeOfMotionFeasibility', summary="For a patient ID, generate a range of motion feasibility assessment for the different joints", response_description="", response_model=PaginationModel[RangeOfMotionAssessment], tags=['Routes', 'PT'])
@version(1, 2)
async def getPatientRangeOfMotionFeasibility(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)

    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )

    ankleInversionLeft = 0
    ankleInversionRight = 0
    ankleEversionLeft = 0
    ankleEversionRight = 0
    footMTPFlexionLeft = 0
    footMTPFlexionRight = 0
    hipAbductionLeft = 0
    hipAbductionRight = 0
    hipAdductionLeft = 0
    hipAdductionRight = 0

    # check if patient data is in the database already
    data = await getPatientDataFromTable(patientID, 'flexion')
    # if it isn't generate it and store it in the database and reply
    if data:
        data = data[0]
        ankleInversionLeft = int(data[2])
        ankleInversionRight = int(data[3])
        ankleEversionLeft = int(data[4])
        ankleEversionRight = int(data[5])
        footMTPFlexionLeft = int(data[6])
        footMTPFlexionRight = int(data[7])
        hipAbductionLeft = int(data[8])
        hipAbductionRight = int(data[9])
        hipAdductionLeft = int(data[10])
        hipAdductionRight = int(data[11])
    else:
        ankleInversionLeft = random.randint(-7, 7)
        ankleInversionRight = random.randint(-7, 7)
        ankleEversionLeft = random.randint(-7, 7)
        ankleEversionRight = random.randint(-7, 7)
        footMTPFlexionLeft = random.randint(-7, 7)
        footMTPFlexionRight = random.randint(-7, 7)
        hipAbductionLeft = random.randint(-7, 7)
        hipAbductionRight = random.randint(-7, 7)
        hipAdductionLeft = random.randint(-7, 7)
        hipAdductionRight = random.randint(-7, 7)
        with conn.cursor() as curs:
            curs.execute(
                f"""insert into public.flexion (id, date, ankleInversionLeft, ankleInversionRight, ankleEversionLeft, ankleEversionRight, footMTPFlexionLeft, footMTPFlexionRight, hipAbductionLeft, hipAbductionRight, hipAdductionLeft, hipAdductionRight) values
                ({patientID.id},'{date.today()}',{ankleInversionLeft}, {ankleInversionRight}, {ankleEversionLeft}, {ankleEversionRight}, {footMTPFlexionLeft}, {footMTPFlexionRight}, {hipAbductionLeft}, {hipAbductionRight}, {hipAdductionLeft}, {hipAdductionRight});"""
            )

    return PaginationModel(data=PaginationData(
        items=[
            RangeOfMotionAssessment(
                ankleInversionLeft=ankleInversionLeft,
                ankleInversionRight=ankleInversionRight,
                ankleEversionLeft=ankleEversionLeft,
                ankleEversionRight=ankleEversionRight,
                footMTPFlexionLeft=footMTPFlexionLeft,
                footMTPFlexionRight=footMTPFlexionRight,
                hipAbductionLeft=hipAbductionLeft,
                hipAbductionRight=hipAbductionRight,
                hipAdductionLeft=hipAdductionLeft,
                hipAdductionRight=hipAdductionRight,
            )
        ]
    ))


@app.post('/getPatientOverallResult', summary="For a patient ID, generate patient overall result for lower extremity RSI, RSI risk message, stability, motion quality, RTP, fall risk, and RSI risk on an integer range from 0-100 with 0 being bad and 100 being excellent", response_description="", response_model=PaginationModel[PatientOverallResult], tags=['Routes', 'PT'])
@version(1, 2)
async def getPatientOverallResult(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)

    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )

    hipFlexorLeft = 0
    hipFlexorRight = 0
    gluteLeft = 0
    gluteRight = 0
    adductorLeft = 0
    adductorRight = 0
    abductorLeft = 0
    abductorRight = 0
    quadricepLeft = 0
    quadricepRight = 0
    hamstringLeft = 0
    hamstringRight = 0
    gastrocnemiousLeft = 0
    gastrocnemiousRight = 0
    soleusLeft = 0
    soleusRight = 0
    footLeft = 0
    footRight = 0
    rsiRiskMessage = 0
    balance = 0
    stability = 0
    motionQuality = 0
    rtp = 0
    fallRisk = 0
    rsiRisk = 0
    balanceGoal = 0
    stabilityGoal = 0
    motionQualityGoal = 0
    rtpGoal = 0
    fallRiskGoal = 0
    rsiRiskGoal = 0

    # check if patient data is in the database already
    data = await getPatientDataFromTable(patientID, 'overallresult')
    # if it isn't generate it and store it in the database and reply
    if data:
        date = data[0]
        hipFlexorLeft = data[2]
        hipFlexorRight = data[3]
        gluteLeft = data[4]
        gluteRight = data[5]
        adductorLeft = data[6]
        adductorRight = data[7]
        abductorLeft = data[8]
        abductorRight = data[9]
        quadricepLeft = data[10]
        quadricepRight = data[11]
        hamstringLeft = data[12]
        hamstringRight = data[13]
        gastrocnemiousLeft = data[14]
        gastrocnemiousRight = data[15]
        soleusLeft = data[16]
        soleusRight = data[17]
        footLeft = data[18]
        footRight = data[19]
        rsiRiskMessage = data[20]
        balance = data[21]
        stability = data[22]
        motionQuality = data[23]
        rtp = data[24]
        fallRisk = data[25]
        rsiRisk = data[26]
        balanceGoal = data[27]
        stabilityGoal = data[28]
        motionQualityGoal = data[29]
        rtpGoal = data[30]
        fallRiskGoal = data[31]
        rsiRiskGoal = data[32]
    else:
        hipFlexorLeft = random.randint(60, 100)
        hipFlexorRight = random.randint(60, 100)
        gluteLeft = random.randint(60, 100)
        gluteRight = random.randint(60, 100)
        adductorLeft = random.randint(60, 100)
        adductorRight = random.randint(60, 100)
        abductorLeft = random.randint(60, 100)
        abductorRight = random.randint(60, 100)
        quadricepLeft = random.randint(60, 100)
        quadricepRight = random.randint(60, 100)
        hamstringLeft = random.randint(60, 100)
        hamstringRight = random.randint(60, 100)
        gastrocnemiousLeft = random.randint(60, 100)
        gastrocnemiousRight = random.randint(60, 100)
        soleusLeft = random.randint(60, 100)
        soleusRight = random.randint(60, 100)
        footLeft = random.randint(60, 100)
        footRight = random.randint(60, 80)
        rsiRiskMessage = f"The {random.choice(['left', 'right'])} {random.choice(['heel','ankle', 'knee', 'hip', 'side'])} has {random.choice(['higher', 'lower'])} biomechanical load and joint load. Some muscles need to improve strength to improve balance and stability."
        balance = random.uniform(0.6, 1)
        stability = random.uniform(0.6, 1)
        motionQuality = random.uniform(0.6, 1)
        rtp = random.uniform(0.6, 1)
        fallRisk = random.uniform(0.6, 1)
        rsiRisk = random.uniform(0.6, 1)
        balanceGoal = 0.7
        stabilityGoal = 0.7
        motionQualityGoal = 0.7
        rtpGoal = 0.7
        fallRiskGoal = 0.7
        rsiRiskGoal = 0.7

        with conn.cursor() as curs:
            makeOverallResultTable()
            curs.execute(
                f"""insert into public.overallresult (id, date, hipFlexorLeft, hipFlexorRight, gluteLeft, gluteRight, adductorLeft, adductorRight, abductorLeft, abductorRight, quadricepLeft, quadricepRight, hamstringLeft, hamstringRight, gastrocnemiousLeft, gastrocnemiousRight, soleusLeft, soleusRight, footLeft, footRight, rsiRiskMessage, balance, stability, motionQuality, rtp, fallRisk, rsiRisk, balanceGoal, stabilityGoal, motionQualityGoal, rtpGoal, fallRiskGoal, rsiRiskGoal) values
                ({patientID.id},'{datetime.today()}',{hipFlexorLeft}, {hipFlexorRight}, {gluteLeft}, {gluteRight}, {adductorLeft}, {adductorRight}, {abductorLeft}, {abductorRight}, {quadricepLeft}, {quadricepRight}, {hamstringLeft}, {  hamstringRight}, {gastrocnemiousLeft}, {gastrocnemiousRight}, {soleusLeft}, {soleusRight}, {footLeft}, {footRight}, '{rsiRiskMessage}', {balance}, {stability}, {motionQuality}, {rtp}, {fallRisk}, {rsiRisk}, {balanceGoal}, {stabilityGoal}, {motionQualityGoal}, {rtpGoal}, {fallRiskGoal}, {rsiRiskGoal});"""
            )

    return PaginationModel(data=PaginationData(
        items=[
            PatientOverallResult(
                patient_id=patientID,
                lowerExtremityRSIRisk=LowerExtremityRisk(
                    hipFlexorLeft=hipFlexorLeft,
                    hipFlexorRight=hipFlexorRight,
                    gluteLeft=gluteLeft,
                    gluteRight=gluteRight,
                    adductorLeft=adductorLeft,
                    adductorRight=adductorRight,
                    abductorLeft=abductorLeft,
                    abductorRight=abductorRight,
                    quadricepLeft=quadricepLeft,
                    quadricepRight=quadricepRight,
                    hamstringLeft=hamstringLeft,
                    hamstringRight=hamstringRight,
                    gastrocnemiousLeft=gastrocnemiousLeft,
                    gastrocnemiousRight=gastrocnemiousRight,
                    soleusLeft=soleusLeft,
                    soleusRight=soleusRight,
                    footLeft=footLeft,
                    footRight=footRight,
                ),
                rsiRiskMessage=rsiRiskMessage,
                balance=balance,
                stability=stability,
                motionQuality=motionQuality,
                rtp=rtp,
                fallRisk=fallRisk,
                rsiRisk=rsiRisk,
                balanceGoal=balanceGoal,
                stabilityGoal=stabilityGoal,
                motionQualityGoal=motionQualityGoal,
                rtpGoal=rtpGoal,
                fallRiskGoal=fallRiskGoal,
                rsiRiskGoal=rsiRiskGoal,
            )
        ]
    ))


@app.post('/getPatientExerciseGrade', summary="For a patient ID and exercise skeleton, generate patient exercise grade (balance, stability, motion quality, rtp, fall risk, and rsi risk)", response_description="", response_model=PaginationModel[ExerciseGrade], tags=['Routes', 'PT'])
@version(1, 2)
async def getPatientExerciseGrade(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID, skeleton: ExerciseSkeleton):
    patient = await getPatientFromDB(patientID)

    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )
    return PaginationModel(data=PaginationData(
        items=[
            ExerciseGrade(
                patient_id=patientID,
                patient_status='online',
                exercise_name='',
                exercise_progress_graph=[
                    ExerciseGraphPoint(
                        performance='Good',
                        progress_start=0,
                        progress_end=0.25
                    ),
                    ExerciseGraphPoint(
                        performance='Average',
                        progress_start=0.25,
                        progress_end=0.5
                    ),
                ],
                total_sec=10,
                number_of_sets=2,
                balance_diagram=[
                    BalanceDiagramPoint(x=random.uniform(
                        0.25, 0.75), y=random.uniform(0.25, 0.75)),
                    BalanceDiagramPoint(x=random.uniform(
                        0.25, 0.75), y=random.uniform(0.25, 0.75)),
                    BalanceDiagramPoint(x=random.uniform(
                        0.25, 0.75), y=random.uniform(0.25, 0.75)),
                ],
                balance=random.uniform(0.6, 1),
                stability=random.uniform(0.6, 1),
                motionQuality=random.uniform(0.6, 1),
                rtp=random.uniform(0.6, 1),
                fallRisk=random.uniform(0.6, 1),
                rsiRisk=random.uniform(0.6, 1),
            )
        ]
    ))


@app.post('/getPatientRTP', summary="For a patient ID, generate a readiness to perform (RTP) summary (fall risk, accuracy, strength, stamina, stability, speed, precision, power, precision, posture, flexibility, exertion, endurance, coordination, control, balance, agility, fatigue, load, metabolic cost, musculoskeletal (msk) stress)", response_description="", response_model=PaginationModel[RTP], tags=['Routes', 'PT'])
@version(1, 2)
async def getPatientRTP(currentActiveUser:Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)

    ankleInversionLeft = 0
    ankleInversionRight = 0
    ankleEversionLeft = 0
    ankleEversionRight = 0
    footMTPFlexionLeft = 0
    footMTPFlexionRight = 0
    hipAbductionLeft = 0
    hipAbductionRight = 0
    hipAdductionLeft = 0
    hipAdductionRight = 0

    # check if patient data is in the database already
    data = await getPatientDataFromTable(patientID, 'flexion')
    # if it isn't generate it and store it in the database and reply
    if data:
        data = data[0]
        ankleInversionLeft = int(data[2])
        ankleInversionRight = int(data[3])
        ankleEversionLeft = int(data[4])
        ankleEversionRight = int(data[5])
        footMTPFlexionLeft = int(data[6])
        footMTPFlexionRight = int(data[7])
        hipAbductionLeft = int(data[8])
        hipAbductionRight = int(data[9])
        hipAdductionLeft = int(data[10])
        hipAdductionRight = int(data[11])
    else:
        ankleInversionLeft = random.randint(-7, 7)
        ankleInversionRight = random.randint(-7, 7)
        ankleEversionLeft = random.randint(-7, 7)
        ankleEversionRight = random.randint(-7, 7)
        footMTPFlexionLeft = random.randint(-7, 7)
        footMTPFlexionRight = random.randint(-7, 7)
        hipAbductionLeft = random.randint(-7, 7)
        hipAbductionRight = random.randint(-7, 7)
        hipAdductionLeft = random.randint(-7, 7)
        hipAdductionRight = random.randint(-7, 7)
        with conn.cursor() as curs:
            makeFlexionTable()
            curs.execute(
                f"""insert into public.flexion (id, date, ankleInversionLeft, ankleInversionRight, ankleEversionLeft, ankleEversionRight, footMTPFlexionLeft, footMTPFlexionRight, hipAbductionLeft, hipAbductionRight, hipAdductionLeft, hipAdductionRight) values
                ({patientID.id},'{date.today()}',{ankleInversionLeft}, {ankleInversionRight}, {ankleEversionLeft}, {ankleEversionRight}, {footMTPFlexionLeft}, {footMTPFlexionRight}, {hipAbductionLeft}, {hipAbductionRight}, {hipAdductionLeft}, {hipAdductionRight});"""
            )

    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )
    return PaginationModel(data=PaginationData(items=[RTP(
        fallRisk=random.randint(0, 20),
        accuracy=random.randint(50, 100),
        strength=random.randint(50, 100),
        stamina=random.randint(50, 100),
        stability=random.randint(50, 100),
        speed=random.randint(50, 100),
        precision=random.randint(50, 100),
        power=random.randint(50, 100),
        posture=random.randint(50, 100),
        flexibility=random.randint(50, 100),
        exertion=random.randint(50, 100),
        endurance=random.randint(50, 100),
        coordination=random.randint(50, 100),
        control=random.randint(50, 100),
        balance=random.randint(50, 100),
        agility=random.randint(50, 100),
        fatigue=random.randint(0, 50),
        load=random.randint(0, 50),
        metabolicCost=random.randint(0, 50),
        mskStress=random.randint(0, 50)
    )]))


@app.post('/getCohortTrendAnalytics', summary="", response_description="", response_model=PaginationModel[CohortTrendAnalytics], tags=['Routes', 'PT'])
@version(1, 1)
async def getCohortTrendAnalytics(token: Annotated[bool, Depends(getCurrentActiveUser)], patientID: PatientID):
    patient = await getPatientFromDB(patientID)

    if not patient:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )

    random.seed(patient[0])
    return PaginationModel(data=PaginationData(items=[
        CohortTrendAnalytics(usagePercentage=random.randint(10, 90)),
    ]))


@app.post('/getPTDashboard', summary="For a PT ID, generate a list of that PT's patients and generate their current program results as an overview. Search attempts to match. Filters are enumerated [None=0,Recovery Progress(H to L)=1, Recovery Progress (L to H)=2, All Active=3, Inactive=4, New=5, Search=6] ", response_description="", response_model=PaginationModel[GroupOverview], tags=['Routes', 'PT'])
@version(1, 2)
async def getPTDashboard(token: Annotated[bool, Depends(getCurrentActiveUser)], ptID: PTID, orgID, cohortID, search: GroupSearch = GroupSearch(search=""), filterTerm: GroupFilter = GroupFilter(filter=0), page: int = 0):
    patientList = await getPatientListFromDB(ptID.id)

    if not patientList:
        return PaginationModel(
            success=False,
            message="Physician does not have any patients.",
            data=PaginationData(items=[]),
        )

    for patient in patientList:
        data = await getPatientDataFromTable(patient.patient_id, 'programresult')
        # if it isn't generate it and store it in the database and reply
        if data:
            data = data[0]
            patient.programResults = ProgramResults(
                patientid=patient.patient_id,
                date=date.today(),
                recoveryProgressCurrent=data[2],
                recoveryProgressGoal=70,
                recoveryProgressDailyDifference=random.randint(-5, 5),
                adherenceCurrent=data[3],
                adherenceGoal=70,
                adherenceProgressDailyDifference=random.randint(-5, 5),
                motionQualityCurrent=data[4],
                motionQualityGoal=70,
                motionQualityDailyDifference=random.randint(-5, 5),
                scheduledProgram=ExerciseList(
                    [
                        Exercise(
                            id=0,
                            program_id=0,
                            program_image='',
                            name="One Leg Hop",
                            description="",
                            sets=3,
                            reps=8,
                            setsCompleted=2,
                            repsCompleted=3
                        ),
                        Exercise(
                            id=1,
                            program_id=0,
                            program_image='',
                            name="Box Step Up",
                            description="",
                            sets=2,
                            reps=4,
                            setsCompleted=2,
                            repsCompleted=4
                        ),
                        Exercise(
                            id=2,
                            program_id=0,
                            program_image='',
                            name="Triple Leg Hop",
                            description="",
                            sets=3,
                            reps=2,
                            setsCompleted=0,
                            repsCompleted=0
                        )
                    ]
                )
            )
        else:
            patient.programResults = ProgramResults(
                patientid=patient.patient_id,
                date=date.today(),
                recoveryProgressCurrent=random.randint(50, 100),
                recoveryProgressGoal=70,
                recoveryProgressDailyDifference=random.randint(-10, 10),
                adherenceCurrent=random.randint(50, 100),
                adherenceGoal=70,
                adherenceProgressDailyDifference=random.randint(-10, 10),
                motionQualityCurrent=random.randint(50, 100),
                motionQualityGoal=70,
                motionQualityDailyDifference=random.randint(-10, 10),
                scheduledProgram=ExerciseList(
                    [
                        Exercise(
                            id=0,
                            program_id=0,
                            program_image='',
                            name="One Leg Hop",
                            description="",
                            sets=3,
                            reps=8,
                            setsCompleted=2,
                            repsCompleted=3
                        ),
                        Exercise(
                            id=1,
                            program_id=0,
                            program_image='',
                            name="Box Step Up",
                            description="",
                            sets=2,
                            reps=4,
                            setsCompleted=2,
                            repsCompleted=4
                        ),
                        Exercise(
                            id=2,
                            program_id=0,
                            program_image='',
                            name="Triple Leg Hop",
                            description="",
                            sets=3,
                            reps=2,
                            setsCompleted=0,
                            repsCompleted=0
                        )
                    ]
                )
            )

    if search.search != "":
        searchLow = search.search.lower()
        patientList = list(
            filter(lambda x: searchLow in x.firstName.lower() or searchLow in x.lastName.lower(), patientList))

    total = len(patientList)
    pages = len(patientList) % 50

    patientList = patientList[0 + 50*page: 50 + 50*page]

    # array of physician's group and institute

    return PaginationModel(data=PaginationData(
        items=[GroupOverview(
            organizationName=[
                "James A. Haley Veteran's Hospital - Tampa, Florida"],
            cohortName="VA Outpatient Physical Therapy",
            organizationImage="None",
            patientList=patientList
        )],
        index=page,
        page=pages,
        total=total,
        size=1
    ))


@app.post('/getPTOrganizations', summary="For a PT ID, generate a list of the organizations that the given PT is apart of.", response_description="", response_model=PaginationModel[Organizations], tags=['Routes', 'PT'])
@version(1, 1)
async def getPTOrganizations(token: Annotated[bool, Depends(getCurrentActiveUser)], ptID: PTID):
    pt = await getPTFromDB(ptID)

    if not pt:
        return PaginationModel(
            success=False,
            message="Patient or PT Not Found",
            data=PaginationData(items=[]),
        )

    if pt[20] is None:
        return PaginationModel(
            success=False,
            message="PT has no organizations.",
            data=PaginationData(items=[]),
        )

    orgs = []
    for org in pt[20]:
        name, img = org.split(':')
        orgs.append(Organizations(name=name, image=img))

    return PaginationModel(data=PaginationData(
        items=orgs,
        index=1,
        page=1,
        total=1,
        size=1,
    ))

app = VersionedFastAPI(app, enable_latest=True)
