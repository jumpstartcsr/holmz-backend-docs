import httpx
import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app as fastapi_app

client = TestClient(fastapi_app)


@pytest.fixture
def test_app() -> FastAPI:
    return fastapi_app


@pytest.fixture
async def async_client(test_app: FastAPI) -> httpx.AsyncClient:
    async with httpx.AsyncClient(app=test_app, base_url="http://test") as ac:
        yield ac


@pytest.mark.asyncio
async def test_get_patient_todays_program_results_invalid_patient(async_client: httpx.AsyncClient):
    response = await async_client.post("/v1.2/getPatientTodaysProgramResults", json={"id": -1})
    assert response.status_code == 200
    assert response.json()["success"] == False
    assert response.json()["message"] == "Patient or PT Not Found"
    assert len(response.json()["data"]["items"]) == 0


@pytest.mark.asyncio
async def test_get_patient_todays_program_results_valid_patient(async_client: httpx.AsyncClient):
    # Replace 1 with a valid patient ID in your database.
    response = await async_client.post("/v1.2/getPatientTodaysProgramResults", json={"id": 1})
    assert response.status_code == 200
    assert response.json()["success"] == True
    assert response.json()["message"] == "success"
    assert len(response.json()["data"]["items"]) > 0
    assert response.json()["data"]["items"][0]["patientid"]["id"] == 1
