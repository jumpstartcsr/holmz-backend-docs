import psycopg2

# Update connection string information

host = "holmz-dev.postgres.database.azure.com"
port = 5432
dbname = "holmz-dev"
user = "devDBAdmin"
password = "k6wQZkLNp5Ae961O"
sslmode = "require"


cursor = None


def setupServer():
    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn.cursor()


def getPatientFromID(cur, id: int):
    cur.execute(
        "SELECT * FROM public.patient WHERE id = %s;", [id])
    patientID = cur.fetchall()[0][1]
    cur.execute(
        "SELECT * FROM public.user WHERE id = %s;", [patientID])
    patientData = cur.fetchall()[0]
    return patientData


def getPTFromID(cur, id: int):
    cur.execute("SELECT * FROM public.physician WHERE id = %s;", [id])
    ptid = 0
    try:
        ptid = cur.fetchall()[0][1]
    except IndexError:
        return 'Invalid pt id'
    cur.execute("SELECT * FROM public.user WHERE id = %s;", [ptid])
    ptdata = cur.fetchall()[0]
    return ptdata


def getPatientsFromPTID(cur, id: int):
    ptuserID = getPTFromID(cur, id)[0]
    cur.execute(
        "SELECT * FROM public.patientmapping WHERE physician_id = %s;", [ptuserID])
    patientList = cur.fetchall()
    return patientList


cursor = setupServer()
print(getPTFromID(cursor, 1))
