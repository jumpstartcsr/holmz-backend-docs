import psycopg2

# Update connection string information

host = "holmz-dev.postgres.database.azure.com"
port = 5432
dbname = "holmz-dev"
user = "devDBAdmin"
password = "k6wQZkLNp5Ae961O"
sslmode = "require"

# Construct connection string

conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
    host, user, dbname, password, sslmode)
conn = psycopg2.connect(conn_string)
print("Connection established")


def getPatientFromID(cur, id: int):
    cur.execute(
        "SELECT * FROM public.patient WHERE id = %s;", [id])
    patientID = cur.fetchall()[0][1]
    cur.execute(
        "SELECT * FROM public.user WHERE id = %s;", [patientID])
    patientData = cur.fetchall()[0]
    return patientData


def getPTFromID(id: int):
    pass


cursor = conn.cursor()

getPatientFromID(cursor, id=1)

# Fetch all rows from table

# cursor.execute("SELECT * FROM inventory;")
# cursor.execute("SELECT datname FROM pg_database;")
# cursor.execute("SELECT * FROM public.user")

# for table in cursor.fetchall():
# print(table)
# print(cursor.fetchall())


# # Print all rows
# rows = cursor.fetchall()
# for row in rows:
#     print("Data row = (%s, %s, %s)" % (str(row[0]), str(row[1]), str(row[2])))

conn.commit()
cursor.close()
conn.close()
