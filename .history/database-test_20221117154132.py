import psycopg2

# Update connection string information

host = "holmz-dev.postgres.database.azure.com"
port = 5432
dbname = "holmz-dev"
user = "devDBAdmin"
password = "Ho3/Ju3G'enaSl{l"
sslmode = "require"

# Construct connection string

conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
    host, user, dbname, password, sslmode)
conn = psycopg2.connect(conn_string)
print("Connection established")

cursor = conn.cursor()

# Fetch all rows from table

# cursor.execute("SELECT * FROM inventory;")
# cursor.execute("SELECT datname FROM pg_database;")
cursor.execute("""SELECT table_name
               FROM information_schema.tables
               WHERE table_schema='public'
               ORDER BY table_name
               """)
for table in cursor.fetchall():
    print(table)
print(cursor.fetchall())


# # Print all rows
# rows = cursor.fetchall()
# for row in rows:
#     print("Data row = (%s, %s, %s)" % (str(row[0]), str(row[1]), str(row[2])))

conn.commit()
cursor.close()
conn.close()
