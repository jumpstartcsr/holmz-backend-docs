# API Approach

- Ignore the full api and the database api for now, database api should come after the XAI api
- Focus on the XAI API

## XAI API

- What are the functions I need to support?
  - Single Leg Support Post (Skeleton)
    - Return Grading skeleton
  - Create Program (Post)
    - Exercises
      - sets
      - reps
  - Grade Program
    - motion quality %+goal
    - recovery progress %+goal
    - adherence %+goal
  - GetPatientMotionQuality (Time)
  - GetPatientRecoveryProgress (Time)
  - GetPatientAdherence (Time)
  - HasPatientAchievedGoals
  - PatientProgramCompletionRate
  - PatientActiveTime
  - PatientCohortActivityDistribution
  - PatientWearableUtilization
  - GetPatientStats
    - RecoveryProgress
    - Adherence
    - Motionquality
    - +Goals
  - GetPatientReport (Time) (Weekly)
  - GetExercisePerformance
    - Balance
    - Stability
      - Postural Sway AP ML
      - Postural Sway Area
    - Test Result (Insight+)
      - Indicator (6.3)
      - Result
    - MotionQuality
    - RTP
    - FallRisk
    - RSI Risk
  - Get Overall Result
    - muscle group (foot, calf, thigh)
    - Balance
    - Stability
    - Motion quality
    - RTP
    - Fall Risk
    - RSI Risk
  - GetRTP
    - Accuracy
    - Strength
    - Stamina
    - Stability
    - Speed
    - Precision
    - Power
    - Posture
    - Flexibility
    - Exertion
    - Endurance
    - Coordination
    - Control
    - Balance
    - Agility
    - Fatigue
    - Load
    - Metabolic Cost
    - Musculoskeletal Stress

## Database API

- What are the functions I need to support?
  - CreateAccount Post
    - FirstName
    - LastName
    - Email
    - Password
    - Return UserID
  - PostSignature Post
  - ChangePassword Post
    - NewPassword
  - GetPateintInjuriesAndSymptoms
  - InjuriesAndSymptoms Post
    - Symptom
    - How
    - When
    - ActivitiesCausePain
    - HowRelievePain
    - ChallengingActivities (tick box)
    - FallenInYear Y/N
    - UnsteadyWhenStand Y/N
    - WorryABTFall Y/N
    - MobilityAid (Tick + other)
    - Treatment to date
    - Other
  - StoreDeviceSerialNumber Post
  - StoreExerciseData Post
    - Skeleton
    - Xosole
    - Pain rating?
      - exercise
      - where (tick + other)
  - Gettodaysprogram
    - exercises
    - motion quality
    - recovery progress
    - adherence
  - storenotification
  - getnotification
  - SetPatientBiomechanicalModel
    - Gender
    - DoB
    - Height
    - Weight
    - Wrist size
    - Hip size
    - Hip Floor LR
    - Knee Floor LR
    - Ankle Floor LR
    - Shoe Size
      - Gender
      - US
      - Size
      - Width
    - Pronation LR
    - Foot Arch LR
  - Set ROM Goals
    - joint
    - current
    - recommendation
    - ROM
    - goal ROM
    - goal timeline (4weeks)
  - Get ROM Goals
  - Get Progress history


I'm going to do something cludgy and edit fastapi versioning file in my pip registry, don't perform an update

<https://github.com/DeanWay/fastapi-versioning/pull/69/commits/ae78b1d1662435d6e36848e90af8576525f64a0d>

```python
try:
 for method in route.methods:
  unique_routes[route.path + "|" + method] = route
except AttributeError:
 unique_routes[route.path] = route
```
