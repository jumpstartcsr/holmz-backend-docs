import psycopg2
from psycopg2 import sql
from jose import jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta

# JWT_SECRET = config('SECRET')
# JWT_ALGORITHM = config('algorithm')


def tokenResponse(token: str):
    return {
        'accessToken': token,
    }


def signJWT(user_id: str) -> dict[str, str]:
    payload = {
        "user_id": user_id,
        "expires": time.time() + 600
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return tokenResponse


def decodeJWT(token: str) -> dict:
    try:
        decodedToken = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decodedToken if decodedToken['expires'] >= time.time() else None
    except:
        return {}


def checkUser(data):
    for user in users:
        if user.email == data.email and user.password == data.password:
            return True
    return False


def connectToDB():
    host = "holmz-dev.postgres.database.azure.com"
    port = 5432
    dbname = "holmz-uat"
    user = "devDBAdmin"
    password = "gzM26qjtqIb3xyh"
    sslmode = "require"

    conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(
        host, user, dbname, password, sslmode)
    conn = psycopg2.connect(conn_string)
    print("Connection established")
    return conn.cursor(), conn


cursor, conn = connectToDB()
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


async def getPatientFromDB(id):
    cursor.execute(
        """SELECT * FROM public.user
        WHERE id = (SELECT user_id FROM public.patient
        WHERE id = %s);""", [id.id]
    )
    data = cursor.fetchone()
    return data


async def getPTFromDB(id):
    cursor.execute(
        """SELECT * FROM public.user
        WHERE id = (SELECT user_id FROM public.physician
        WHERE id = %s);""", [id.id]
    )
    data = cursor.fetchone()
    return data


async def getPatientListFromDB(id):
    cursor.execute(
       """SELECT * FROM public.patient LEFT JOIN
       public.patientmapping ON id = patient_id
       WHERE physician_id = %s;""", [id]
    )
    data = cursor.fetchall()
    return data


async def getPatientDataFromTable(id, table, altTitle=None):
    with conn.cursor() as curs:
        if not altTitle:
            curs.execute(
                sql.SQL("SELECT * FROM public.{} WHERE id = %s")
                .format(sql.Identifier(table)),
                [id.id])
        else:
            curs.execute(
                sql.SQL("SELECT * FROM public.{table} WHERE {field} = %s")
                .format(
                    table=sql.Identifier(table),
                    field=sql.Identifier(altTitle)
                ), [id]
            )
        data = curs.fetchone()
        return data


class PID(object):
    id = 3


async def login(userEmail, userPassword):
    user = await getPatientDataFromTable(userEmail, 'user', altTitle='email')
    if not user:
        return {}
    if not pwd_context.verify(userPassword, user[7]):
        return {}
    to_encode = {"exp": '', "sub": str('tst')}
    return jwt.encode(to_encode, 'secret', algorithm="HS256")


p_id = PID()
# print(datetime.now() + timedelta(minutes=60))
# print(asyncio.run(login("test@gm.co", "best")))
# print(asyncio.run(getPatientFromDB(p_id)))
# print(asyncio.run(getPatientDataFromTable(p_id, 'flexion')))

to_token = {'exp': datetime.utcnow() + timedelta(minutes=1), 'sub': 'hi'}
to_token = {'sub': 'hi'}
token = jwt.encode(to_token, 'sec', algorithm='HS256')
data = jwt.decode(token, 'sec', algorithms=['HS256'])
print(data)


# pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
# print(pwd_context.hash('best'))
